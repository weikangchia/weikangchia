### Hi there, I am Wei Kang 👷‍ - Once a Programmer, Always a Programmer

I am a Software Engineer working in a small island in the Southeast Asia. I am passionate about technology and love to build things. Apart from building things, I usually spend my time reading and trying out new technology.

### 📝 Latest Medium Articles
<!-- MEDIUM-LIST:START -->
- [KrakenD: How to Deploy to Kubernetes](https://medium.com/javarevisited/krakend-how-to-deploy-to-kubernetes-bd1bf2173b0?source=rss-f534096234c5------2)
- [Free Docker Desktop Alternative for Mac](https://medium.com/javarevisited/free-docker-desktop-alternative-for-mac-c3845d8a2345?source=rss-f534096234c5------2)
- [Practical Guide to JUnit 5 Parameterized Tests](https://blog.gds-gov.tech/practical-guide-to-junit-5-parameterized-tests-38675ca749b0?source=rss-f534096234c5------2)
- [Part 1: How To Add OpenAPI 3.0 And Swagger To Spring Boot Application](https://medium.com/javarevisited/part-1-how-to-add-openapi-3-0-and-swagger-to-spring-boot-application-35c96422e94b?source=rss-f534096234c5------2)
- [3 Different Ways to Import Modules in TypeScript](https://javascript.plainenglish.io/3-different-ways-to-import-modules-in-typescript-7cd02d8e0da3?source=rss-f534096234c5------2)
<!-- MEDIUM-LIST:END -->

### 🤖 Open Source Projects
[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=weikangchia&repo=gitcg)](https://github.com/weikangchia/gitcg)
[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=weikangchia&repo=op-converter)](https://github.com/weikangchia/op-converter)
[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=weikangchia&repo=pre-commit-hooks-plantuml)](https://github.com/weikangchia/pre-commit-hooks-plantuml)
[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=weikangchia&repo=github-android-card)](https://github.com/weikangchia/github-android-card)

### 📱 Mobile Apps
[![Readme Card](https://github-android-card.vercel.app/api/gplay-apps/com.appspot.khubite.sgsupermarket)](https://play.google.com/store/apps/details?id=com.appspot.khubite.sgsupermarket)
[![Readme Card](https://github-android-card.vercel.app/api/gplay-apps/khubite.appspot.com.sgtvguide)](https://play.google.com/store/apps/details?id=khubite.appspot.com.sgtvguide)

### ⚒ Languages and Tools

[<img align="left" alt="Java" width="26px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.13.0/icons/java.svg" />][github]
[<img align="left" alt="JavaScript" width="26px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.13.0/icons/javascript.svg" />][github]
[<img align="left" alt="TypeScript" width="26px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.13.0/icons/typescript.svg" />][github]
[<img align="left" alt="Node.js" width="26px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.13.0/icons/node-dot-js.svg" />][github]
[<img align="left" alt="Angular" width="26px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.13.0/icons/angularjs.svg" />][github]
[<img align="left" alt="Jest" width="26px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.13.0/icons/jest.svg" />][github]
[<img align="left" alt="Spring Boot" width="26px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.13.0/icons/spring.svg" />][github]
[<img align="left" alt="AWS" width="26px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.13.0/icons/amazonaws.svg" />][github]
[<img align="left" alt="Serverless" width="26px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.13.0/icons/serverless.svg" />][github]
[<img align="left" alt="Kubernetes" width="26px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.13.0/icons/kubernetes.svg" />][github]
[<img align="left" alt="WhiteSource" width="26px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.13.0/icons/whitesource.svg" />][github]
[<img align="left" alt="Visual Studio Code" width="26px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.13.0/icons/visualstudiocode.svg" />][github]
[<img align="left" alt="IntelliJ" width="26px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.13.0/icons/intellijidea.svg" />][github]

<br/><br/>

### 🍀 Stats

![GitHub stats](https://github-readme-stats.vercel.app/api?username=weikangchia&show_icons=true)

![GitHub Language stats](https://github-readme-stats.vercel.app/api/top-langs/?username=weikangchia&layout=compact&hide=javascript,html,css)

![Wwakatime stats](https://github-readme-stats-taupe-two.vercel.app/api/wakatime?username=weikangchia&langs_count=5)

### 📮 Connect with Me

[<img align="left" alt="weikangchia | Medium" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.13.0/icons/medium.svg" />][medium]
[<img align="left" alt="weikangchia | LinkedIn" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.13.0/icons/linkedin.svg" />][linkedin]

[medium]: https://weikangchia.medium.com
[linkedin]: https://linkedin.com/in/weikangchia
[github]: https://github.com/weikangchia
